<?php
namespace Ecg\Sniffs\BcSniffs;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Tokens;

class TranslateCJKSniff implements PHP_CodeSniffer_Sniff
{
    CONST CN_HAN_CHAR = '/\p{Han}+/u';
    CONST JP_KANJI_CHAR = '/[\x{4E00}-\x{9FBF}]/u';
    CONST JP_HIRAGANA_CHAR = '/[\x{3040}-\x{309F}]/u';
    CONST JP_KATAKANA_CHAR = '/[\x{30A0}-\x{30FF}\x{31F0}-\x{31FF}]/u';
    CONST KR_HANGUL_CHAR = '/[\x{1100}-\x{11FF}\x{3130}-\x{318F}\x{AC00}-\x{D7AF}]/u';
    CONST CJK_SYMBOLS_PUNCTUATION = '/[\x{3000}-\x{303F}]/u';

    protected function getStrTokens()
    {
        return array_merge(PHP_CodeSniffer_Tokens::$stringTokens, [T_HEREDOC, T_NOWDOC]);
    }

    public function register()
    {
        return array_merge([T_STRING], $this->getStrTokens());
    }

    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {

        $tokens = $phpcsFile->getTokens();

        $string = $tokens[$stackPtr]['content'];
        $isDirectTranslation = $this->isDirectTranslation($string);
        if ($isDirectTranslation) {
            $phpcsFile->addError('Direct translation detected: %s.', $stackPtr, 'translation', array($string));
        }
    }

    /**
     * @param string $string string
     *
     * @return bool
     */
    protected function isDirectTranslation($string)
    {
        if(preg_match(self::CN_HAN_CHAR, $string)
            || preg_match(self::JP_KANJI_CHAR, $string)
            || preg_match(self::JP_HIRAGANA_CHAR, $string)
            || preg_match(self::JP_KATAKANA_CHAR, $string)
            || preg_match(self::KR_HANGUL_CHAR, $string)
            || preg_match(self::CJK_SYMBOLS_PUNCTUATION, $string)
        ) {
            return true;
        }

        return false;
    }

}
