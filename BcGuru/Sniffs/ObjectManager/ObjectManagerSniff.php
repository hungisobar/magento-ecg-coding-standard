<?php

namespace BcGuru\Sniffs\ObjectManager;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class ObjectManagerSniff implements PHP_CodeSniffer_Sniff
{
    public $objectManager = 'objectmanager';

    public function register()
    {
        return array(T_STRING);
    }

    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $var = $tokens[$stackPtr]['content'];

        $varLower = strtolower($var);
        $fileName = strtolower($phpcsFile->getFilename());
        if (strpos($fileName, 'command') !== false) {
            return;
        } else {
            if (strpos($varLower, $this->objectManager) !== false) {
                $phpcsFile->addError('Direct use of %s detected.', $stackPtr, 'objectManager', array($var));
            }
        }
    }
}
