<?php

namespace BcGuru\Sniffs\ThrowNewCatch;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class ThrowNewCatchSniff implements PHP_CodeSniffer_Sniff
{

    protected $numTokens = 0;

    protected $tokens = array();

    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return array
     */
    public function register()
    {
        return array(T_CATCH);

    }//end register()


    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $this->tokens = $tokens;
        $this->numTokens = count($tokens);
        $token = $tokens[$stackPtr];

        // Skip statements without a body.
        if (isset($token['scope_opener']) === false) {
            return;
        }

        $types = array(T_CATCH); //1
        $start = ($token['parenthesis_opener']); //2
        $end = ($token['scope_closer'] - 1); //3
        $exclude = true; //4

        $hasThrow = $this->findNext(
            $types, //1
            $start, //2
            $end, //3
            $exclude, //4
            'throw' //5
        );

        $hasNew = $this->findNext(
            $types, //1
            $start, //2
            $end, //3
            $exclude, //4
            'new' //5
        );

        $hasDuplicateException = $this->findNext(
            $types, //1
            $start, //2
            $end, //3
            $exclude, //4
            'Exception' //5
        );

        if ($hasDuplicateException < 2) {
            // No duplicate exception
            return;
        }

        if (($hasThrow !== false && $hasNew === false) || ($hasThrow === false)) {
            return;
        }


        $error = 'Duplicate type throw new exception detected.';
        $phpcsFile->addError($error, $stackPtr, 'Detected');

    }//end process()

    /**
     * Returns the position of the next specified token(s).
     *
     * If a value is specified, the next token of the specified type(s)
     * containing the specified value will be returned.
     *
     * Returns false if no token can be found.
     *
     * @param int|array $types The type(s) of tokens to search for.
     * @param int $start The position to start searching from in the
     *                           token stack.
     * @param int $end The end position to fail if no token is found.
     *                           if not specified or null, end will default to
     *                           the end of the token stack.
     * @param bool $exclude If true, find the next token that is NOT of
     *                           a type specified in $types.
     * @param string $value The value that the token(s) must be equal to.
     *                           If value is omitted, tokens with any value will
     *                           be returned.
     * @param bool $local If true, tokens outside the current statement
     *                           will not be checked. i.e., checking will stop
     *                           at the next semi-colon found.
     *
     * @return int|bool
     * @see    findPrevious()
     */
    protected function findNext(
        $types, //1
        $start, //2
        $end = null, //3
        $exclude = false, //4
        $value = null, //5
        $local = false //6
    )
    {
        $types = (array)$types;
        $tokens = $this->tokens;
        if ($end === null || $end > $this->numTokens) {
            $end = $this->numTokens;
        }

        $exceptions = array();
        for ($i = $start; $i < $end; $i++) {
            $found = (bool)$exclude;
            foreach ($types as $type) {
                if ($tokens[$i]['code'] === $type) {
                    $found = !$exclude;
                    break;
                }
            }

            if ($found === true) {
                $tokenContent = $tokens[$i]['content'];
                if ($value === null) {
                    return $i;
                } else if ($tokenContent === $value && $value !== 'Exception') {
                    return $i;
                } else if (strpos($tokenContent, 'Exception') !== false) {

                    $afterException = $tokens[$i + 1]['content'];

                    if ($afterException !== '\\') {
                        if ($afterException === ' ' || $afterException === '(') {
                            $afterException = '-';
                        }
                        $beforeException = $tokens[$i - 1]['content'];
                        $exception = $tokenContent;
                        $exceptionString = $beforeException . $exception . $afterException;
                        if (in_array($exceptionString, $exceptions)) {
                            return $i;
                        } else {
                            $exceptions[] = $exceptionString;
                        }
                    }
                }
            }


            if ($local === true && $tokens[$i]['code'] === T_SEMICOLON) {
                break;
            }
        }//end for

        return false;

    }//end findNext()
}
