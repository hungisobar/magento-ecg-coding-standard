<?php

namespace BcGuru\Sniffs\VariableCamelCases;

use PHP_CodeSniffer_Sniff;
use PHP_CodeSniffer_File;

class VariableCamelCasesSniff implements PHP_CodeSniffer_Sniff
{
    public $underscore = '_';

    public function register()
    {
        return array(T_VARIABLE);
    }

    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();

        $var = $tokens[$stackPtr]['content'];

        $varTrim = ltrim($var, '$_');

        if (strpos($varTrim, $this->underscore) !== false) {
            $phpcsFile->addError('Change to camel case: %s.', $stackPtr, 'underscore', array($var));
        }
    }
}
