<?php

namespace BcGuru\Sniffs\ForLoopWithTestFunctionCall;

use Generic_Sniffs_CodeAnalysis_ForLoopWithTestFunctionCallSniff as OriForLoopWithTestFunctionCallSniff;

use PHP_CodeSniffer_File;
use PHP_CodeSniffer_Tokens;

class ForLoopWithTestFunctionCallSniff extends OriForLoopWithTestFunctionCallSniff
{
    /**
     * Registers the tokens that this sniff wants to listen for.
     *
     * @return int[]
     */
    public function register()
    {
        return array(
            T_FOR,
//            T_FOREACH
        );

    }//end register()
    /**
     * Processes this test, when one of its tokens is encountered.
     *
     * @param PHP_CodeSniffer_File $phpcsFile The file being scanned.
     * @param int                  $stackPtr  The position of the current token
     *                                        in the stack passed in $tokens.
     *
     * @return void
     */
    public function process(PHP_CodeSniffer_File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $token  = $tokens[$stackPtr];

        // Skip invalid statement.
        if (isset($token['parenthesis_opener']) === false) {
            return;
        }

        $next = ++$token['parenthesis_opener'];
        $end  = --$token['parenthesis_closer'];

        $position = 0;

        if ($token['type'] === 'T_FOREACH') {
            $position = 1;
        }

        for (; $next <= $end; ++$next) {
            $code = $tokens[$next]['code'];
            if ($code === T_SEMICOLON || $code === T_AS) {
                ++$position;
            }

            if ($position < 1) {
                continue;
            } else if ($position > 1) {
                break;
            } else if ($code !== T_VARIABLE && $code !== T_STRING) {
                continue;
            }

            // Find next non empty token, if it is a open curly brace we have a
            // function call.
            $index = $phpcsFile->findNext(PHP_CodeSniffer_Tokens::$emptyTokens, ($next + 1), null, true);

            if ($tokens[$index]['code'] === T_OPEN_PARENTHESIS) {

                $error = 'Avoid function calls in a ' . strtoupper($token['content']) . ' loop test part';
                $phpcsFile->addError($error, $stackPtr, 'NotAllowed');
                break;
            }
        }//end for

    }//end process()


}//end class
